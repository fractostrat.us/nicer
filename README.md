# Nicer

## A (N)ixOS R(icer)
Inspired by [Luke Smith][]'s [LARBS][] and [David Wood][]'s [veritas][] with my own opinions liberally sprinkled in.


[Luke Smith]: https://lukesmith.xyz
[LARBS]: https://larbs.xyz/
[David Wood]: https://davidtw.co/
[veritas]: https:/github.com/davidtwco